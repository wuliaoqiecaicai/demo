/**
 * \
 * 提供api
 * doGet doPost service
 */
public abstract class Servlet {

    public void service(RequestHandler requestHandler, ResponseHandler responseHandler) {
        if (requestHandler.getMethod().equals(Method.Get)) {
            doGet(requestHandler, responseHandler);

        }
        if (requestHandler.getMethod().equals(Method.Post)) {
            doPost(requestHandler, responseHandler);

        }

    }

    protected abstract void doPost(RequestHandler requestHandler, ResponseHandler responseHandler);

    protected abstract void doGet(RequestHandler requestHandler, ResponseHandler responseHandler);
}
