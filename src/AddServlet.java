import java.io.IOException;
import java.util.Objects;

public class AddServlet extends Servlet {
    @Override
    public void doGet(RequestHandler request, ResponseHandler response) {
        Integer a = null, b = null;
        try {
            String parameterA = (String) request.getParameter("a");
            if (!Objects.isNull(parameterA)) {

                a = Integer.parseInt(parameterA);
            }
            String parameterB = (String) request.getParameter("b");
            if (!Objects.isNull(parameterB)) {
                b = Integer.parseInt(parameterB);
            }
            if (Objects.isNull(a) || Objects.isNull(b)) {
                response.write("a or b is empty");
            } else response.write("a=" + a + ",b=" + b + ",a+b=" + (a + b));
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void doPost(RequestHandler myRequest, ResponseHandler myResponse) {
        doGet(myRequest, myResponse);
    }
}
