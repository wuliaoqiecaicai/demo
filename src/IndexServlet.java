import java.io.IOException;

public class IndexServlet extends Servlet {
    @Override
    public void doGet(RequestHandler myRequest, ResponseHandler myResponse) {
        try {
            myResponse.write("Hello, World");
//            myResponse.write("a:" + myRequest.getParameter("a"));
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void doPost(RequestHandler myRequest, ResponseHandler myResponse) {
        doGet(myRequest, myResponse);
    }
}