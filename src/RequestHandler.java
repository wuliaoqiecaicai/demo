import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 对请求进行处理
 */
public class RequestHandler {
    private String url;//请求地址
    //    private String method;//请求方法
    private Method method = Method.Get;
    private Map<String, Object> parameter = new HashMap<>();

    public RequestHandler(InputStream inputStream) throws IOException {
        String httpRequst = "";
        byte[] httpRequestBytes = new byte[1024];
        int length = 0;
        if ((length = inputStream.read(httpRequestBytes)) > 0) {
            httpRequst = new String(httpRequestBytes, 0, length);
        }
        String[] httpHeadS = httpRequst.split("\r\n");
        String[] httpHead = httpHeadS[0].split(" ");
        System.out.println(httpHead);
        setMethod(httpHead[0].trim());
        String[] urlComplete = httpHead[1].trim().split("\\?");
        setUrl(urlComplete[0]);
        if (urlComplete.length > 1) {
            String s = urlComplete[1];
            if ((!Objects.isNull(s)) && (!"".equals(s))) {
                String[] parameters = s.split("&");
                for (String p : parameters) {
                    String[] split = p.split("=");
                    parameter.put(split[0], split[1]);
                }

            }
        }
        System.out.println(this.toString());
    }

    public String getUrl() {
        return url;
    }

    public Object getParameter(String parameter) {
        return this.parameter.get(parameter);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(String methodName) {

        switch (methodName.toLowerCase()) {
            case "get":
                setMethod(Method.Get);
                break;
            case "post":
                setMethod(Method.Post);
                break;
            default:
                break;
        }
    }

    public void setMethod(Method method) {
        this.method = method;
    }
//    public String getMethod() {
//        return method;
//    }
//
//    public void setMethod(String method) {
//        this.method = method;
//    }

    @Override
    public String toString() {
        return "RequestHandler{" +
                "url='" + url + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}

enum Method {
    Get, Post
}
