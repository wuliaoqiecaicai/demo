import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * tomcat的处理流程：把url对应处理的Servlet关系形成，解析HTTP协议，封装请求、响应对象，
 * 利用反射实例化具体的Servlet进行处理即可；
 */
public class Tomcat {
    private Integer port = 8080;
    private Map<String, String> urlServletMapping = new HashMap<>();
    private ResponseHandler responseHandler = null;
    private RequestHandler requestHandler = null;
    private ServerSocket serverSocket = null;
    private Socket socket = null;

    public Tomcat(Integer port) {
        this.port = port;
        initServletMapping();
    }

    public void initServletMapping() {
        for (ServletMapping servletMapping : ServletMappingConfig.servletMappingList) {
            urlServletMapping.put(servletMapping.getUrl(), servletMapping.getClazz());
        }
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("The Server is Starting.....");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void start() {
        try {
            while (true) {
                socket = serverSocket.accept();
                requestHandler = new RequestHandler(socket.getInputStream());
                responseHandler = new ResponseHandler(socket.getOutputStream());
                dispatch(requestHandler, responseHandler);
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public void dispatch(RequestHandler requestHandler, ResponseHandler responseHandler) {
        String clazz = urlServletMapping.get(requestHandler.getUrl());
        try {
            Class<Servlet> servletClass = (Class<Servlet>) Class.forName(clazz);
            // Servlet servlet = servletClass.getConstructor().newInstance(requestHandler, responseHandler);
            Servlet servlet = servletClass.getConstructor().newInstance();
            servlet.service(requestHandler, responseHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
