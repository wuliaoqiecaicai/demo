import java.util.ArrayList;
import java.util.List;

public class ServletMappingConfig {
    public static List<ServletMapping> servletMappingList = new ArrayList<>();

    static {

        servletMappingList.add(new ServletMapping("index", "", "IndexServlet"));
        servletMappingList.add(new ServletMapping("index", "/index", "IndexServlet"));
        servletMappingList.add(new ServletMapping("index", "index", "IndexServlet"));
        servletMappingList.add(new ServletMapping("add", "add", "AddServlet"));
        servletMappingList.add(new ServletMapping("add", "/add", "AddServlet"));
        servletMappingList.add(new ServletMapping("add", "add", "AddServlet"));

        servletMappingList.add(new ServletMapping("add", "add", "AddServlet"));
        servletMappingList.add(new ServletMapping("add", "/add", "AddServlet"));
        servletMappingList.add(new ServletMapping("add", "add", "AddServlet"));


        servletMappingList.add(new ServletMapping("mult", "mult", "MultServlet"));
        servletMappingList.add(new ServletMapping("mult", "/mult", "MultServlet"));
        servletMappingList.add(new ServletMapping("mult", "mult", "MultServlet"));
        // servletMappingList.add(new ServletMapping("myblog", "Host: localhost", "MyBlog"));

    }

}
